ARG CI_REGISTRY
FROM debian:bookworm-backports

RUN <<EOF
apt-get update
apt-get -y upgrade
apt-get -y install ruby ruby-dev build-essential zlib1g-dev locales
sed --in-place '/en_US.UTF-8/s/^#//' /etc/locale.gen
locale-gen
gem install jekyll bundler
EOF

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
