# Introduction

This project creates a docker image with jekyll installed.

The image can be used to create sites using jekyll.

This repository is mirrored to https://gitlab.com/sw4j-net/jekyll
